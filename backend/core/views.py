import logging

from django.shortcuts import render, redirect
from django.utils import timezone

from core.helper import get_client_ip, get_query_log_string, get_result_log_string
from core.search import evaluate_query_whoosh, load_index

logger = None
whoosh_index = None


def index(request):
    return render(request, 'index.html')


def search(request, query=None, page=1, logger=logger, whoosh_index=whoosh_index):
    #load index and initialize logger during the first search
    logger = logger or logging.getLogger(__name__)
    whoosh_index = whoosh_index or load_index()
    
    if query is None:
        querystring = request.POST['query']
    else:
        querystring = query

    logger.info(get_query_log_string(get_client_ip(request), querystring))
    print('query', querystring)

    matches = evaluate_query_whoosh(whoosh_index, querystring, page_num=page)
    return render(request, 'results.html', {'matches': matches, 'query': querystring})


def result(request, query, doc_id):
    logger.info(get_result_log_string(get_client_ip(request), query, doc_id))
    forwarding_wiki_url = 'https://en.wikipedia.org/?curid={}'.format(doc_id)
    return redirect(forwarding_wiki_url)
