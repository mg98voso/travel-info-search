import xapian

from whoosh.qparser import QueryParser, MultifieldParser
from whoosh.filedb.filestore import FileStorage
from whoosh.fields import Schema, TEXT, KEYWORD, ID, STORED
from whoosh.analysis import StemmingAnalyzer
from whoosh.sorting import FieldFacet
from whoosh.query import Term, And, Or

from core.helper import get_preview

from travelinfo.settings import XAPIAN_DATABASE_PATH, WHOOSH_DATABASE_PATH

def load_index():
    whoosh_storage = FileStorage(WHOOSH_DATABASE_PATH)
    index = whoosh_storage.open_index()
    return index

def get_schema_whoosh():
    schema = Schema(
        identifier=ID(stored=True),
        title=TEXT(stored=True, analyzer=StemmingAnalyzer()),
        alt=KEYWORD(commas=True, scorable=True, lowercase=True, analyzer=StemmingAnalyzer()),
        type=STORED(),
        text=TEXT(analyzer=StemmingAnalyzer()),
        summary=TEXT(analyzer=StemmingAnalyzer(), stored=True),
        tags=KEYWORD(commas=True, scorable=True, lowercase=True, analyzer=StemmingAnalyzer()),
        content=KEYWORD(commas=True, scorable=True, lowercase=True, analyzer=StemmingAnalyzer()),
        meta=KEYWORD(commas=True, scorable=True, lowercase=True, analyzer=StemmingAnalyzer())
    )

    return schema


def evaluate_query_xapian(querystring):
    xapian_db = xapian.Database(XAPIAN_DATABASE_PATH)

    # stemmer = xapian.Stem('en')
    # terms = [term.strip().lower() for term in querystring.split(' ')]
    # query = xapian.Query(xapian.Query.OP_AND_MAYBE, terms)
    # Set up a QueryParser with a stemmer and suitable prefixes
    queryparser = xapian.QueryParser()
    queryparser.set_default_op(xapian.Query.OP_NEAR)
    queryparser.set_stemmer(xapian.Stem("en"))
    queryparser.set_stemming_strategy(queryparser.STEM_SOME)
    # Start of prefix configuration.
    queryparser.add_prefix("title", "S")
    queryparser.add_prefix("text", "XD")
    # End of prefix configuration.

    # And parse the query
    query = queryparser.parse_query(querystring)

    # Use an Enquire object on the database to run the query
    enquire = xapian.Enquire(xapian_db)
    enquire.set_query(query)

    offset = 0
    pagesize = 10

    return enquire.get_mset(offset, pagesize)


def evaluate_query_whoosh(index, querystring, page_num=1, page_len=10):
    parser = MultifieldParser(['text', 'summary', 'title', 'alt', 'tags', 'content', 'meta'],
                              schema=get_schema_whoosh(), fieldboosts=get_weight_dict())
    query = parser.parse(querystring)
    print(query)

    with index.searcher() as searcher:
        # results = searcher.search(query, groupedby=FieldFacet('meta'))
        results = searcher.search_page(query, page_num, page_len)
        # print(results.groups())
        matches = {
            'page_num': results.pagenum,
            'page_count': results.pagecount,
            'is_last_page': results.is_last_page(),
            'results': []
        }
        # print('key terms', results.key_terms('summary'))
        for result in results:
            result_fields = result.fields()
            matches['results'].append({
                'title': result_fields.get('title', ''),
                'preview': get_preview(result_fields.get('summary', '')),
                'docid': result_fields.get('identifier', ''),
                'rank': result.score
            })
    return matches


class FieldWeights:
    TITLE = 0.45
    ALT = 1.0
    TEXT = 0.1
    SUMMARY = 1.0
    TAGS = 1.0
    CONTENT = 1.0
    META = 1.0


def get_weight_dict():
    weight_dict = {
        'text': FieldWeights.TEXT,
        'summary': FieldWeights.SUMMARY,
        'title': FieldWeights.TITLE,
        'alt': FieldWeights.ALT,
        'tags': FieldWeights.TAGS,
        'content': FieldWeights.CONTENT,
        'meta': FieldWeights.META
    }
    return weight_dict


def get_query_from_querystring(querystring):
    querystring_parts = [part.strip() for part in querystring.split(' ')]

    text_query_terms = [Term('text', term) for term in querystring_parts]
    text_query = And(text_query_terms, boost=FieldWeights.TEXT)

    title_query_terms = [Term('title', term) for term in querystring_parts]
    title_query = Or(title_query_terms, boost=FieldWeights.TITLE)

    alt_query_terms = [Term('alt', term) for term in querystring_parts]
    alt_query = And(alt_query_terms, boost=FieldWeights.ALT)

    tags_query_terms = [Term('tags', term) for term in querystring_parts]
    tags_query = Or(tags_query_terms, boost=FieldWeights.TAGS)

    content_query_terms = [Term('content', term) for term in querystring_parts]
    content_query = And(content_query_terms, boost=FieldWeights.CONTENT)

    meta_query_terms = [Term('meta', term) for term in querystring_parts]
    meta_query = And(meta_query_terms, boost=FieldWeights.META)

    query = Or([text_query, title_query, alt_query, tags_query, content_query, meta_query])
    return query
