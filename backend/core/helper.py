import csv
import math
import sys

from django.utils import timezone

csv.field_size_limit(sys.maxsize)


def parse_csv_file(datapath, charset='utf8'):
    """
    Parse a CSV file.
    Assumes the first row has field names.
    Yields a dict keyed by field name for the remaining rows.
    """
    with open(datapath, encoding=charset) as fd:
        reader = csv.DictReader(fd)
        for row in reader:
            yield row


def get_distance(coordinate1, coordinate2):
    """
    :returns: distance in kilometers
    """
    lat1 = math.radians(coordinate1['latitude'])
    lat2 = math.radians(coordinate2['latitude'])
    lon1 = math.radians(coordinate1['longitude'])
    lon2 = math.radians(coordinate2['longitude'])

    distance_step = math.sin(lat1) * math.sin(lat2) + math.cos(lat1) * math.cos(lat2) * math.cos(lon2 - lon1)
    if distance_step > 1.0:
        distance_step = 1.0

    distance = math.acos(distance_step) * 6371.0

    return distance


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0].strip()
    else:
        ip = request.META.get('REMOTE_ADDR').strip()
    return ip


def get_preview(summary):
    if len(summary) <= 300:
        return summary

    return summary[:300] + '...'


def get_query_log_string(client_ip, querystring):
    return '[{}][{}] SEARCH query:"{}"'.format(timezone.now().strftime('%d.%m.%Y %H:%M:%S.%f'), client_ip, querystring)


def get_result_log_string(client_ip, querystring, doc_id):
    return '[{}][{}] RESULT query:"{}" id:{}'.format(timezone.now().strftime('%d.%m.%Y %H:%M:%S.%f'), client_ip,
                                                     querystring, doc_id)
