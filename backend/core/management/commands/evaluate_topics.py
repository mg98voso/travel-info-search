import json

from django.core.management.base import BaseCommand, CommandError

from core.search import evaluate_query_whoosh, load_index

from datetime import datetime

from os import listdir, path

index = load_index()    # should be still better than loading a seperate index for every query...


class Command(BaseCommand):
    help = 'Evaluate the search topics from json file'

    def add_arguments(self, parser):
        parser.add_argument('--topics', dest='ptopics',
                            help='Path to the topics json file')
        parser.add_argument('--evaluations', dest='pevaluation',
                            help='Path to (optional) pre-existing evaluations and for the current one')
        parser.add_argument('--outputname', dest='outputname',
                            help='Filename for the current evaluation, defaults to %d%m%y_%H%M%S.json')

    def handle(self, *args, **options):
        ptopics = options.get('ptopics', None)
        pevaluation = options.get('pevaluation', None)
        outname = options.get('outputname', None)
        outname = outname or datetime.now().strftime("%d%m%y_%H%M%S.json")
        if not outname.endswith(".json"):
            outname += ".json"

        if not path.isfile(ptopics):
            raise CommandError('You have to specify a path to the json file')
        if not path.isdir(pevaluation):
            raise CommandError('You have to specify an output directory for the evaluation')

        with open(ptopics) as topics_json:
            topics = json.load(topics_json)
        
        old_judgements = dict()
        for old_name in [n for n in listdir(pevaluation) if n.endswith('.json')]:
            with open(path.join(pevaluation, old_name)) as old_file:
                jms = json.load(old_file)
            for j in jms:
                try:    #ignore other json-files, f.e. the topic-listing
                    old_judgements[(j['topic_id'], j['document_id'])] = j['relevance']
                except:
                    pass
        
        new_judgements = []
        
        print("Starting evaluation...\nTo quit, enter 'exit' instead of a relevance-judgement. (Assignments will be saved.)")
        exit = False
        
        for topic in topics:
            query = topic['query']
            print(query)
            matches = evaluate_query_whoosh(index, query)['results']

            for match in matches:
                print('\tscore:', match['rank'], '\n\tdocid:', match['docid'], '\n\ttitle:', match['title'])
                try:
                    relevance = old_judgements[(topic['topic_id'], match['docid'])]
                    print(f"relevance: {relevance}")
                except:
                    print(match['preview'])
                    user_input = input("\nEnter relevance-judgement: ")
                    if user_input == "exit":
                        exit = True
                        break
                    else:
                        relevance = user_input
                    print('\n')
                new_judgements.append({
                    'topic_id': topic['topic_id'],
                    'document_id': match['docid'],
                    'relevance': relevance
                })
            if exit:
                break
        with open(path.join(pevaluation, outname), 'w') as output:
            json.dump(new_judgements, output, indent=4)
