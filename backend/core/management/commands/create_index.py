import xapian
import json

from django.core.management.base import BaseCommand, CommandError

from core.helper import parse_csv_file
from travelinfo.settings import XAPIAN_DATABASE_PATH


class Command(BaseCommand):
    help = 'DEPRECATED Create the xapian search index'

    def add_arguments(self, parser):
        parser.add_argument('--path', dest='path',
                            help='Path to the csv file to index')

    def handle(self, *args, **options):
        path = options.get('path', None)

        if not path:
            raise CommandError('You have to specify a path to a csv file')

        xapian_db_writable = xapian.WritableDatabase(XAPIAN_DATABASE_PATH, xapian.DB_CREATE_OR_OPEN)

        termgenerator = xapian.TermGenerator()
        termgenerator.set_stemmer(xapian.Stem("en"))

        for fields in parse_csv_file(path):
            # 'fields' is a dictionary mapping from field name to value.
            # Pick out the fields we're going to index.
            title = fields.get('TITLE', u'')
            type = fields.get('TYPE', 'u')
            identifier = fields.get('ID', u'')
            tags = fields.get('TAGS', 'u')
            content = fields.get('CONTENT', 'u')
            text = fields.get('TEXT', 'u')

            # We make a document and tell the term generator to use this.
            doc = xapian.Document()
            termgenerator.set_document(doc)

            # Index each field with a suitable prefix.
            termgenerator.index_text(title, 1, 'S')
            termgenerator.index_text(text, 1, 'XD')

            # Index fields without prefixes for general search.
            termgenerator.index_text(title)
            termgenerator.increase_termpos()
            termgenerator.index_text(content)

            # Store all the fields for display purposes.
            doc.set_data(json.dumps(fields))

            # We use the identifier to ensure each object ends up in the
            # database only once no matter how many times we run the
            # indexer.
            idterm = u"Q" + identifier
            doc.add_boolean_term(idterm)
            xapian_db_writable.replace_document(idterm, doc)
