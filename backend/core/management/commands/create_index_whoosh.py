import xapian
import json

from whoosh.filedb.filestore import FileStorage

from django.core.management.base import BaseCommand, CommandError

from core.helper import parse_csv_file
from core.search import get_schema_whoosh
from travelinfo.settings import WHOOSH_DATABASE_PATH


class Command(BaseCommand):
    help = 'Create the whoosh search index'

    def add_arguments(self, parser):
        parser.add_argument('--path', dest='path',
                            help='Path to the csv file to index')

    def handle(self, *args, **options):
        path = options.get('path', None)

        if not path:
            raise CommandError('You have to specify a path to a csv file')

        whoosh_storage = FileStorage(WHOOSH_DATABASE_PATH)
        index = whoosh_storage.create_index(schema=get_schema_whoosh())
        writer = index.writer()

        counter = 1
        for fields in parse_csv_file(path):
            if counter % 1000 == 0:
                print(counter, 'articles processed')
            # 'fields' is a dictionary mapping from field name to value.
            # Pick out the fields we're going to index.
            identifier = fields.get('ID', '')
            title = fields.get('TITLE', '')
            alt = fields.get('ALT', '')
            article_type = fields.get('TYPE', '')
            type_boost = int(article_type or 1)
            text = fields.get('TEXT', '')
            summary = fields.get('SUMMARY', '')
            tags = fields.get('TAGS', '')
            content = fields.get('CONTENT', '')
            meta = fields.get('META', '')

            writer.add_document(identifier=identifier,
                                title=title,
                                alt=alt,
                                type=article_type,
                                text=text,
                                summary=summary,
                                tags=tags,
                                content=content,
                                meta=meta,
                                _boost=type_boost)  # POIs are more important than the rest
            counter += 1

        writer.commit()
