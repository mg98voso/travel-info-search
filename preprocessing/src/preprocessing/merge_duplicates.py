import sys

def main(pathIn, pathOut, SEPARATOR=','):
    with open(pathIn, "r") as fin, open(pathOut, "w") as fout:
        index = dict()
        line = fin.readline()
        pos = 0
        print("scanning document...\n")
        while not line == '':
            idlen = line.find(SEPARATOR)
            if idlen < 0:   #line doesn't contain separator
                continue
            id = int(line[:idlen])
            if id in index:
                index[id].append(pos)
            else:
                index[id] = [pos]
            pos = fin.tell()
            line = fin.readline()
        print("sorting index...\n")
        sorted_index = sorted(index.items(), key = lambda i: i[0])
        print("merging lines...\n")
        for i, positions in sorted_index:
            idstr = str(i)
            lines = []
            for p in positions:
                fin.seek(p)
                lines.append(fin.readline()[len(idstr)+1:-1])    # cut off id+separator and ending newline
            fout.write(idstr)
            fout.write(SEPARATOR)
            fout.write(SEPARATOR.join(lines))
            fout.write("\n")
        print("complete.\n")

if __name__ == "__main__":
    pathIn = sys.argv[1]
    pathOut = sys.argv[2]
    sep = sys.argv[3] if len(sys.argv) > 3 else ','
    main(pathIn, pathOut, sep)
