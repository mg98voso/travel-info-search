#extracts relevant articles from the wikipedia xml

import xml.etree.ElementTree as etree
import re
from gensim.corpora.wikicorpus import remove_markup


class Link:

    def __init__(self, target, paragraph, text):
        self.target = target
        self.paragraph = paragraph
        self.text = text

    def parseLink(initstr):
        match = re.match("\[\[\:?([^#\|\]]+)(#[^\|\]]*)?(\|[^\]]*)?\]\]", initstr)
        if match:
            target, paragraph, text = match.groups()
            if target.startswith("Category:"):
                return False
            if paragraph:
                paragraph = paragraph[1:]
            if text:
                text = text[1:]
                if re.search("[<>\|]", text):
                    text = ''
            return Link(target, paragraph, text)
        else:
            return False


class Index:
    def __init__(self, indexfile, title2id=False, id2title=False):
        self.t2id = dict()
        self.id2t = dict()
        for line in indexfile:
            bytes, id, *title = (line[:-1]).split(":")
            title = ":".join(title)
            title = title.replace("&amp;", "&").replace("&quot;", '"')   #titles are stored differently in indexfile
            if title2id:
                self.t2id[title] = int(id)
            if id2title:
                self.id2t[int(id)] = title
    
    def getID(self, title, fail=-1):
        try:
            title = title[0].upper()+title[1:]
            title = title.replace('_', ' ')
            title = title.strip()
            return self.t2id[title]
        except:
            return fail
    
    def getTitle(self, id, fail=''):
        try:
            return self.id2t[id]
        except:
            return fail


def _clean(txt):
    txt = remove_markup(txt)
    txt = re.sub("[\n]+", "\n", txt)
    txt = re.sub("(?:\([^a-zA-Z\(\)]*\)|^\n|''+)", "", txt)
    txt = re.sub(" +", " ", txt)
    return txt


class Article:

    def __init__(self, title, id, ns, redirect='', text= ''):
        self.title = title
        self.id = id
        self.ns = ns
        self.redirect = redirect
        self.text = text or ''  #handle 'None'-input
        self._cats = []
        self._links = []
        self._paragraphs = []
        
    def getCategories(self):
        if not self._cats:
            self._cats = re.findall(r"\[\[Category\:([^\]\|\[\n]*)[^\]\[\n]*\]\]", self.text)
        return self._cats

    def getLinks(self):
        if not self._links:
            links = [Link.parseLink(link) for link in re.findall(r"\[\[[^\]\[\n]+\]\]", self.text)]
            self._links = [link for link in links if link]
        return self._links

    def getParagraphs(self):
        if not self._paragraphs:
            self._paragraphs = re.findall(r"==([^=\n]+)==[^=]", self.text)
        return self._paragraphs
    
    def getSumm(self):
        end = self.text.find("==")
        if end == -1:
            return _clean(self.text)
        else:
            return _clean(self.text[:end])
    
    #return everything except summ
    def getBody(self):
        start = self.text.find("==")
        if start == -1:
            return ""
        else:
            return _clean(self.text[start:])


def strip_tag_name(t):
    idx = k = t.rfind("}")
    if idx != -1:
        t = t[idx + 1:]
    return t


class ArticleIterator:

    def __init__(self, xmlfile):
        self.xmlfile = xmlfile
        
    def __iter__(self):
        self.xmliter = etree.iterparse(self.xmlfile, events=('start', 'end'))
        return self
        
    def __next__(self):
        while(True):
            event, elem = next(self.xmliter)
            tname = strip_tag_name(elem.tag)
    
            if event == 'start':
                if tname == 'page':
                    title = ''
                    id = -1
                    redirect = ''
                    inrevision = False
                    ns = 0
                    txt = ''
                elif tname == 'revision':
                    # Do not pick up on revision id's
                    inrevision = True
            else:
                if tname == 'title':
                    title = elem.text
                elif tname == 'id' and not inrevision:
                    id = int(elem.text)
                elif tname == 'redirect':
                    redirect = elem.attrib['title']
                elif tname == 'ns':
                    ns = int(elem.text)
                elif tname == 'text':
                    txt = elem.text
                elif tname == 'page':
                    elem.clear()
                    return Article(title, id, ns, redirect, txt)


