import csv
from random import choice
import webbrowser

ids = []
with open('/media/matze/TOSHIBA EXT/tis_data/lsi.csv') as lsifile:
    reader = csv.reader(lsifile)
    for ID, *values in reader:
        ids.append(ID)

with open('random_relevants.csv', "a+") as outfile:
    writer = csv.writer(outfile)
    exit = False
    while not exit:
        ID = choice(ids)
        webbrowser.open(f"https://en.wikipedia.org/?curid={ID}")
        uinput = input(f"{ID}: geo→ 1, poi→ 2: ")
        if uinput == "exit":
            exit = True
            break
        else:
            writer.writerow([ID, uinput])
