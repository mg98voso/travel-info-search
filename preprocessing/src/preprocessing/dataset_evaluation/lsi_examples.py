import csv
import webbrowser
import sys
from .wikiparser import Index

with open('/media/matze/TOSHIBA EXT/tis_data/small_index.txt') as f:
    index = Index(f, id2title=True)

topics = []
for i in range(300):
    topics.append([])

with open('/media/matze/TOSHIBA EXT/tis_data/lsi_full.csv', "r") as file:
    reader = csv.reader(file)
    count = 0
    for ID, *values in reader:
        for i, v in enumerate(values):
            topics[i].append((int(ID), float(v)))
        count += 1
        if count % 10000 == 0:
            print(count)
            for i in range(300):
                topics[i] = sorted(topics[i], key=lambda t:-t[1])[:3]
            if count % 100000 == 0:
                for i, l in enumerate(topics):
                    print(f"{i}: {'; '.join([index.getTitle(item[0]) for item in l])}")
    for i in range(300):
        topics[i] = sorted(topics[i], key=lambda t:-t[1])[:3]
    for i, l in enumerate(topics):
        print(i)
        for ID, v in l:
            webbrowser.open(f"https://en.wikipedia.org/wiki/?curid={ID}")
        input()
