import csv
from random import choice
import sys
from .wikiparser import Index
import webbrowser

ids = []
with open('/media/matze/TOSHIBA EXT/tis_data/small_index.txt') as idxfile:
    index = Index(idxfile, id2title=True)
keys = list(index.id2t.keys())

with open('true_randoms.csv', "a+") as outfile:
    writer = csv.writer(outfile)
    exit = False
    while not exit:
        ID = choice(keys)
        webbrowser.open(f"https://en.wikipedia.org/?curid={ID}")
        uinput = input(f"{ID} - {index.getTitle(ID)}\ngeo→ 1, poi→ 2: ")
        if uinput == "exit":
            exit = True
            break
        else:
            writer.writerow([ID, uinput])
