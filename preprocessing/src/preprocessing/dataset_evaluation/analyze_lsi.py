import csv
from random import sample
import numpy as np
import matplotlib.pyplot as plt
"""
with open('/media/matze/TOSHIBA EXT/tis_data/relevance.csv') as relevants:
    relreader = csv.reader(relevants)
    rels = []
    irrels = []
    next(relreader)
    print("reading catdist-relevance..")
    for ID, hascoords, georel, poirel in relreader:
        ID = ID
        hascoords = True if hascoords == "True" else False
        poirel = int(poirel) if poirel.isnumeric() else 0
        if hascoords and poirel >= 2 and poirel <= 7:
            rels.append(ID)
        else:
            irrels.append(ID)
print(f"{len(rels)} most likely relevant Articles collected.")
print("reducing irrelevant-list..")
irrels = sample(irrels, len(rels))
sampled = sorted([(ID, True) for ID in rels] + [(ID, False) for ID in irrels], key=lambda l:int(l[0]))
relvals = dict()
irrelvals = dict()
count = 0
print("collecting lsi-values..")
with open('/media/matze/TOSHIBA EXT/tis_data/lsi_full.csv') as lsif, open("big_jplusv.csv", "w") as output:
    lsireader = csv.reader(lsif)
    writer = csv.writer(output)
    count = 0
    i = iter(sampled)
    ID1, rel = next(i)
    for ID2, *values in lsireader:
        if ID1 == ID2:
            writer.writerow([ID1, rel] + values)
            try:
                ID1, rel = next(i)
            catch:
                break
        count += 1
        print("\033[H\033[J")
        print(count)
"""
"""

topics_r = dict()
rc = 0
topics_i = dict()
ic = 0

with open("big_jplusv.csv") as f:
    reader = csv.reader(f)
    for ID, rel, *values in reader:
        ID = int(ID)
        rel = True if rel == "True" else False
        values = sorted([(i, float(v)) for i, v in enumerate(values)], key=lambda tup:-tup[1])
        values = [(v, rank) for rank, (index, v) in sorted(enumerate(values), key=lambda tup:tup[1][0])]
        if rel:
            for i in range(300):
                topics_r[i] = topics_r.get(i, np.array([0,0])) + values[i]
            rc += 1
        else:
            for i in range(300):
                topics_i[i] = topics_i.get(i, np.array([0,0])) + values[i]
            ic += 1

for i in range(300):
    topics_r[i] /= rc
    topics_i[i] /= ic

topics = dict()

for i in range(300):
    relr = topics_r[i][1]
    relv = topics_r[i][0]
    irrelr = topics_i[i][1]
    irrelv = topics_i[i][0]
    topics[i] = [i, relr, irrelr, relr-irrelr, relv, irrelv, relv-irrelv]

for i, relr, irrelr, diffr, relv, irrelv, diffv in sorted(topics.values(), key=lambda item: - item[3]):
    print(i)
    print(f" r:{relr:.2f} v:{relv:.2f}")
    print(f" r:{irrelr:.2f} v:{irrelv:.2f}")

interesting = [i for i, relr, irrelr, diffr, relv, irrelv, diffv in topics.values() if abs(diffr) > 20]
print(interesting)
"""

interesting = [2, 4, 5, 7, 8, 9, 10, 12, 14, 15, 17, 20, 22, 23, 24, 26, 27, 28, 29, 33, 36, 37, 38, 39, 40, 41, 46, 52, 57, 67, 70, 71, 74, 85, 87, 90, 94, 114, 138, 175, 221, 224, 225, 250, 272]

topics = dict()
sample_size = 0
count = 0

with open("/media/matze/TOSHIBA EXT/tis_data/big_jplusv.csv") as f:
    reader = csv.reader(f)
    for ID, rel, *values in reader:
        if count % 100 == 0:
            ID = int(ID)
            rel = True if rel == "True" else False
            values = sorted([(i, float(v)) for i, v in enumerate(values)], key=lambda tup:-tup[1])
            values = [(v, rank) for rank, (index, v) in sorted(enumerate(values), key=lambda tup:tup[1][0])]
            for i in interesting:
                topics[i] = topics.get(i, []) + [(rel, values[i])]
            sample_size += 1
            print("\033[H\033[J")
            print(sample_size)
        count += 1

corpus_size = 58535 #5853540   #TODO
sample_factor = (corpus_size - (sample_size/2)) / (sample_size / 2)
chunk_offsets = [corpus_size * (n/20) for n in range(1, 10)] + [corpus_size]
print(chunk_offsets)
for i in sorted(interesting):
    print(i)
    values = []
    ranks = []
    pos = 0
    neg = 0
    offsets = iter(chunk_offsets)
    offset = next(offsets)
    for rel, (v, r) in sorted(topics[i], key=lambda item:item[1][1]):
        if rel:
            pos += 1
        else:
            neg += sample_factor
        if pos + neg >= offset:
            ranks.append((pos/(pos+neg), r))
            try:
                offset = next(offsets)
            except:
                break
    print((pos, neg))
    pos = 0
    neg = 0
    offsets = iter(chunk_offsets)
    offset = next(offsets)
    for rel, (v, r) in sorted(topics[i], key=lambda item:-item[1][0]):
        if rel:
            pos += 1
        else:
            neg += sample_factor
        if pos + neg >= offset:
            values.append((pos/(pos+neg), v))
            try:
                offset = next(offsets)
            except:
                break
    print((pos, neg))
    plt.subplot(211)
    rdescs = [f"r < {r}" for p, r in ranks]
    print(rdescs)
    rns = [p for p, r in ranks]
    print(rns)
    plt.bar(rdescs, rns)
    plt.subplot(212)
    vdescs = [f"v > {v:.2f}" for p, v in values]
    print(vdescs)
    vns = [p for p, r in values]
    print(vns)
    plt.bar(vdescs, vns)
    print('\n')
    plt.show()



