import csv
from itertools import chain

assignments = dict()
with open('random_relevants.csv') as rels, open('true_randoms.csv') as tots:
    r1 = csv.reader(rels)
    r2 = csv.reader(tots)
    for ID, j in chain(r1, r2):
        assignments[ID] = j if j in ['1', '2'] else '0'
assignments = sorted(assignments.items(), key=lambda i: int(i[0]))

with open('/media/matze/TOSHIBA EXT/tis_data/lsi_full.csv') as lsi_values, open('jplusv.csv', "w") as out:
    lsir = csv.reader(lsi_values)
    writer = csv.writer(out)
    aiter = iter(assignments)
    aID, aJ = next(aiter)
    aID = int(aID)
    count = 0
    for ID, *values in lsir:
        ID = int(ID)
        if ID > aID:    #just to be sure
            try:
                aID, aJ = next(aiter)
                aID = int(aID)
            except:
                break
        if aID == ID:
            writer.writerow([ID, aJ] + values)
            try:
                aID, aJ = next(aiter)
                aID = int(aID)
            except:
                break
            count += 1
            print("\033[H\033[J")
            print(count)
        
