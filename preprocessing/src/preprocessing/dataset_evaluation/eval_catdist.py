from random import choice
import sys
sys.path.append('../')
from wikiparser import Index
import csv
import bz2
import webbrowser

MAX_LEVEL = 9



class RelEntries:
    complete = dict()
    coords = []
    types = dict()
    types['georel'] = dict()
    types['poirel'] = dict()
    
    def __init__(self, id, hascoords, georel, poirel):
        self.id = int(id)
        self.hascoords = True if hascoords == "True" else False
        self.georel = False if georel == "False" else int(georel)
        self.poirel = False if poirel == "False" else int(poirel)
    
    def add(id, hascoords, georel, poirel):
        new = RelEntries(id, hascoords, georel, poirel)
        RelEntries.complete[new.id] = new
        if new.hascoords:
            RelEntries.coords.append(new)
        if new.georel:
            if new.georel in RelEntries.types['georel']:
                RelEntries.types['georel'][new.georel].append(new)
            else:
                RelEntries.types['georel'][new.georel] = [new]
        if new.poirel:
            if new.poirel in RelEntries.types['poirel']:
                RelEntries.types['poirel'][new.poirel].append(new)
            else:
                RelEntries.types['poirel'][new.poirel] = [new]


def evalList(l, csvf, question):
    counter = 0
    seen = []
    while counter < min(50, len(l)):
        e = choice(l)
        if not e in seen:
            counter += 1
            seen.append(e)
            title = index.getTitle(e.id)
            webbrowser.open(f"http://en.wikipedia.org/wiki/{title}")
            judgement = input(f"{question} {title}: ")
            csvf.writerow([e.id, judgement])



if __name__ == "__main__":

    indexpath = sys.argv[1]
    relpath = sys.argv[2]
    output = sys.argv[3]

    indexfile = bz2.open(indexpath, "rt")
    index = Index(indexfile, id2title=True)
    indexfile.close()



    with open(relpath, "r") as relevants:
        relreader = csv.reader(relevants)
        next(relreader) #header
        for id, hascoords, georel, poirel in relreader:
            RelEntries.add(id, hascoords, georel, poirel)



    with open(output, "a+") as ofile:
        writer = csv.writer(ofile)
        #writer.writerow(["Coordinates"])
        #evalList(RelEntries.coords, writer, "GEO or POI?")
        for i in range(2, MAX_LEVEL+1):
            if input(f"geo, {i}/{MAX_LEVEL} - write 'skip' to skip, anything else to continue: ") == "skip":
                continue
            else:
                writer.writerow(["georel", str(i)])
                evalList(RelEntries.geo[i], writer, "GEO?")
        for i in range(2, MAX_LEVEL+1):
            if input(f"poi, {i}/{MAX_LEVEL} - write 'skip' to skip, anything else to continue: ") == "skip":
                continue
            else:
                writer.writerow(["poirel", str(i)])
                evalList(RelEntries.poi[i], writer, "POI?")
