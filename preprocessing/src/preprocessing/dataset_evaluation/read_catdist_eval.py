import csv
import bz2
from operator import itemgetter
from eval_catdist import RelEntries

import sys
sys.path.append('../')

from wikiparser import Index

DOCS = ("../../../evaluation/eval_mg.csv", "../../../evaluation/eval_td.csv")


indexpath = sys.argv[1]
relpath = sys.argv[2]


#read Index
indexfile = bz2.open(indexpath, "rt")
index = Index(indexfile, id2title=True)
indexfile.close()

#read relevance-measurements
with open(relpath, "r") as relevants:
    relreader = csv.reader(relevants)
    next(relreader) #header
    for id, hascoords, georel, poirel in relreader:
        RelEntries.add(id, hascoords, georel, poirel)

#read relevance-judgements
results = dict()
for filename in DOCS:
    with open(filename, "r") as file:
        results[filename] = dict()
        csvr = csv.reader(file)
        for first, second in csvr:
            if not first.isnumeric():
                current_type = first
                current_typerank = int(second)
                if (current_type, current_typerank) not in results[filename]:
                    results[filename][(current_type, current_typerank)] = []
            elif not second == "":  #ignore ids with missing judgement
                results[filename][(current_type, current_typerank)].append((int(first), int(second)))    #(id, judgement)


mresults = dict()


for key in sorted(set([key for evalsession in results for key in results[evalsession]]), key=itemgetter(0, 1)):
    print(f"\n{key}")
    for evalsession in results:
        if key in results[evalsession]:
            l = results[evalsession][key]
            precision = len([1 for ID, j in l if j > 0]) / len(l) if l else 0
            print(f"{precision:.2f} ({len(l)})  - {evalsession}")
            mresults[key] = mresults.get(key, []) + l
    if key in mresults:
        total = len(RelEntries.types[key[0]][key[1]])
        totalgt = len([e for e in RelEntries.types[key[0]][key[1]] if e.hascoords])
        l = mresults[key]
        positives = len([1 for ID, j in l if j > 0])
        precision = positives / len(l) if l else 0
        print(f"avg: {precision:.2f} * {total:,} ~ {precision * total:,.0f}")
        l_coords = [i for i in l if RelEntries.complete[i[0]].hascoords]
        cpositives = len([1 for ID, j in l_coords if j > 0])
        recall = cpositives / positives
        precision = cpositives / len(l_coords) if l_coords else 0
        print(f"only coordinates:\n\trecall: {recall:.2f}\n\tprecision: {precision:.2f}\n\t{totalgt}\n\t~ {precision*totalgt:,.0f}")

