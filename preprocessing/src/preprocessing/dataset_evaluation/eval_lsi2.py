import csv
import matplotlib.pyplot as plt

data = []
stats = dict()
topics = dict()
total = 0
with open('jplusv.csv') as inp:
    reader = csv.reader(inp)
    for ID, j, *values in reader:
        j = int(j)
        stats[j] = stats.get(j, 0) + 1
        total += 1
        ts = [i for i, v in sorted(enumerate(values), key=lambda v:v[1])[:10]]
        data.append([int(ID), j] + ts)
        for t in ts:
            topics[t] = topics.get(t, []) + [j]

for j in stats:
    print(f"{j}: {stats[j]} - {stats[j] / total:.2f}")

for topic, items in sorted(topics.items(), key=lambda item:item[0]):
    print(f"{topic}: {len(items)}, {len([i for i in items if i])/len(items):.2f}, {len([i for i in items if i == 2])/len(items):.2f}")
