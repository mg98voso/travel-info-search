from gensim.corpora.dictionary import Dictionary
from gensim.models.lsimodel import LsiModel

from gensim.corpora.textcorpus import TextCorpus
from whoosh.analysis import StemmingAnalyzer
import bz2
from wikiparser import ArticleIterator
from gensim.parsing.preprocessing import STOPWORDS

class Wikicorpus(TextCorpus):

    def getstream(self):
        num_texts = 0
        self.input.seek(0)
        iterator = ArticleIterator(self.input)
        for article in iterator:
            if article.ns == 0 and not article.redirect:
                num_texts += 1
                if num_texts % 100000 == 0:
                    print(f"{num_texts} gelesen")
                yield article
        self.length = num_texts
    
    def get_texts(self):
        tokenize = StemmingAnalyzer(stoplist = STOPWORDS)
        for article in self.getstream():
            if article.ns == 0 and not article.redirect:
                yield [token.text for token in tokenize(article.getSumm())] + [token.text for token in tokenize(article.getBody())] + [token.text for token in tokenize(" ".join(article.getCategories()))]
    
    def __len__(self):
        self.length = sum(1 for _ in self.get_texts())
        return self.length

dump = bz2.open("/media/matze/TOSHIBA EXT/enwiki-20190501-pages-articles-multistream.xml.bz2", "rt")

print("Building corpus...")
corpus = Wikicorpus(dump, dictionary=Dictionary.load("/media/matze/TOSHIBA EXT/wikitopics/wikidictionary"))
print("Saving dictionary...")
corpus.dictionary.save("/media/matze/TOSHIBA EXT/wikitopics/wikidictionary")

print("Training model...")
model = LsiModel(corpus, num_topics=200, id2word=corpus.dictionary)

print("Saving model...")
model.save("/media/matze/TOSHIBA EXT/wikitopics/model")

dump.close()
