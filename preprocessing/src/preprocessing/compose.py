import bz2
import csv
import sys
import itertools
from wikiparser import ArticleIterator
    
    
def main(articlesPath, aliasesPath, exPath, outPath):
    articles = bz2.open(articlesPath, 'rt', encoding='utf-8')
    expanded = open(exPath, 'r')
    aliases = open(aliasesPath, 'r')
    out = open(outPath, 'w')
    

    writer = csv.writer(out)
    writer.writerow(["ID","TITLE","ALT","TYPE","SUMMARY","TEXT","TAGS","CONTENT","META"])
    
    artiter = iter(ArticleIterator(articles))
    art = next(artiter)
    aliasreader = csv.reader(aliases)
    alias_id, *aliasnames = next(aliasreader)
    alias_id = int(alias_id)
    
    exreader = csv.reader(expanded)
    
    count = 0
    tcount = 1
    
    for ID, TYPE, META in exreader:
        ID = int(ID)
        while art.id < ID:
            art = next(artiter)
            tcount += 1
        if art.id != ID:
            print(f"Error at id {ID}")
            return
        while alias_id < ID:
            alias_id, *aliasnames = next(aliasreader)
            alias_id = int(alias_id)
            aliasnames = [an.lower() for an in aliasnames]
        writer.writerow([str(ID), art.title, ",".join(set(aliasnames)), TYPE, art.getSumm(), art.getBody(), ",".join(art.getCategories()), ",".join(art.getParagraphs()), META])
        count += 1
        if count % 100000 == 0:
            print(f"{count} Articles extracted, {tcount} seen.")
    print(f"{count} Articles extracted, {tcount} seen.")
    
    articles.close()
    aliases.close()
    expanded.close()
    out.close()




if __name__ == "__main__":
    articlesPath = sys.argv[1]
    relevancePath = sys.argv[2]
    aliasesPath = sys.argv[3]
    exPath = sys.argv[4]
    outPath = sys.argv[5]
    main(articlesPath, relevancePath, exPath, outPath)
