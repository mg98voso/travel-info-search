import sys
import csv
import gc
import bz2

def main(pathIndex, pathRedirects, pathLinksIn, pathLinksTo, pathLinksFrom, pathAliases):

    redirect = dict()
    redirected = dict()

    indexfile = bz2.open(pathIndex, mode='rt', encoding='utf-8')
    flinks_in = open(pathLinksIn, "r", newline = "")
    flinks_to = open(pathLinksTo, "w", newline = "")
    flinks_from = open(pathLinksFrom, "w", newline = "")
    faliases = open(pathAliases, "w", newline = "")

    print("\nreading index...\n")

    index = dict()
    for line in indexfile:
        bytes, id, *title = (line[:-1]).split(":")
        index[int(id)] = ":".join(title)
    indexfile.close()

    with open(pathRedirects, "r") as fredirects:
        redirectreader = csv.reader(fredirects)
        for source, target in redirectreader:
            source = int(source)
            target = int(target)
            redirect[source] = target

    #resolve redirection-chains
    def finalredirection(source, visited = []):
        if (not source in redirect) or (source in visited):
            return source
        else:
            return finalredirection(redirect[source], visited + [source])

    print("normalizing redirections...\n")
    for source in redirect.keys():
        finaltarget = finalredirection(source)  
        redirect[source] = finaltarget  
        redirected[finaltarget] = redirected.get(finaltarget, []) + [source]

    print("extracting aliases from redirections...\n")
    aliaswriter = csv.writer(faliases)
    for target in redirected:
        aliaswriter.writerow([target] + [index[t] for t in redirected[target]])    #write titles of all articles that redirect to this id

    index = None
    gc.collect()    #pray to the gods of garbage collection

    links_from = dict()
    aliases = dict()

    count = 0
    threshold = 1000000

    linkreader = csv.reader(flinks_in)
    ltwriter = csv.writer(flinks_to)
    lfwriter = csv.writer(flinks_from)

    def resolve(link):
        try:
            return redirect[link]
        except:
            return link

    print("processing links...\n")
    next(linkreader)
    for title, id, *links in linkreader:
        links = [link.split("|") for link in links]
        links = [[resolve(int(link[0]))] + link[1:] for link in links if int(link[0]) >= 0]
        links = [link for link in links if link[0] >= 0]
        ltwriter.writerow([id] + [str(link[0]) for link in links])
        for i, (target, *alias) in enumerate(links):
            if len(alias) > 1:  #link'text' should not contain "|"
                alias = []
            elif len(alias) == 1:
                aliases[target] = aliases.get(target, []) + alias
            links_from[target] = links_from.get(target, []) + [id, i]    #keep track of the links' rank
        count += 1
        if count >= threshold:
            for key in aliases:
                aliaswriter.writerow([key] + aliases[key])
            for key in links_from:
                lfwriter.writerow([key] + links_from[key])
            count = 0
            aliases = dict()
            links_from = dict()
            gc.collect()
            print("chunk written...\n")
    for key in aliases:
        aliaswriter.writerow([key] + aliases[key])
    for key in links_from:
        lfwriter.writerow([key] + links_from[key])
    flinks_in.close()
    flinks_to.close()
    flinks_from.close()
    faliases.close()


if __name__ == "__main__":
    pathIndex = sys.argv[1]
    pathRedirects = sys.argv[2]
    pathLinksIn = sys.argv[3]
    pathLinksTo = sys.argv[4]
    pathLinksFrom = sys.argv[5]
    pathAliases = sys.argv[6]
    main(pathIndex, pathRedirects, pathLinksIn, pathLinksTo, pathLinksFrom, pathAliases)
