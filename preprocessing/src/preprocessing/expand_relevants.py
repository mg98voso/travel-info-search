import bz2
import csv
import sys
import itertools
from wikiparser import Index

def getType(coords, georel, poirel):
    if poirel:
        if poirel <= 5 or (poirel <= 7 and coords):
            return 2
    elif georel:
        if georel <= 7 and coords:
            return 1
    else:
        return None

def main(indexPath, relevancePath, linksfromPath, exPath):
    relevance = open(relevancePath, 'r')
    linksFrom = open(linksfromPath, 'r')
    out = open(exPath, 'w')
    
    #build index of relevant articles
    relreader = csv.reader(relevance)
    with bz2.open(indexPath, 'rt', encoding='utf-8') as indexfile:
        index = Index(indexfile, id2title=True)
    relindex = dict()
    next(relreader) #header
    for ID,hascoords,georel,poirel in relreader:
        ID = int(ID)
        hascoords = int(hascoords)
        georel = int(georel) if georel.isnumeric() else False
        poirel = int(poirel) if poirel.isnumeric() else False
        TYPE = getType(hascoords, georel, poirel)
        if TYPE:
            relindex[ID] = (index.getTitle(ID), TYPE)
    index = None
    
    
    #building linktree
    linkreader = csv.reader(linksFrom)
    lf = dict()
    lt = dict()
    for target, *sources in linkreader:
        if int(target) in relindex:
            sources = zip(*[itertools.islice(sources, i, None, 2) for i in range(2)])
            sources = [int(i[0]) for i in sources if (int(i[0]) in relindex and int(i[1]) < 10)]
            lf[int(target)] = sources

    for target in lf:
        targetc = len(lf[target])
        for src in lf[target]:
            if len(lf.get(src, [])) / targetc < 1:
                lt[src] = lt.get(src, []) + [target]

    lf = None

    def expand(artid):
        directlinks = lt.get(artid, [])
        return set.union(set(directlinks), *[expand(link) for link in directlinks])
    
    
    writer = csv.writer(out)
    
    count = 0
    
    for ID in sorted(relindex.keys()):
        TYPE = relindex[ID][1]
        writer.writerow([str(ID), TYPE, ",".join([relindex[e][0] for e in expand(ID)])])
        count += 1
        if count % 100000 == 0:
            print(f"{count} Articles processed")
    print(f"Total: {count} Articles processed")
    
    relevance.close()
    linksFrom.close()
    out.close()



if __name__ == "__main__":
    indexPath = sys.argv[1]
    relevancePath = sys.argv[2]
    linksfromPath = sys.argv[3]
    outPath = sys.argv[4]
    main(indexPath, relevancePath, linksfromPath, outPath)
