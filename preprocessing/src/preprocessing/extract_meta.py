import bz2
import codecs
import sys
import re
import csv
from wikiparser import Article, Link, ArticleIterator, Index


def main(pathWikiXML, pathIndex, pathCats, pathRedirects, pathLinksTo, pathSmallIndex):
    indexfile = bz2.open(pathIndex, mode='rt', encoding='utf-8')
    xmlfile = bz2.open(pathWikiXML, mode='rt', encoding='utf-8')
    supercatfile = open(pathCats, "w", newline = "")
    linkfile = open(pathLinksTo, "w", newline = "")
    redirectfile = open(pathRedirects, "w", newline = "")
    smallindex = open(pathSmallIndex, "w")

    print("\nloading index...\n")
    index = Index(indexfile, title2id=True)

    indexfile.close()

    catwriter = csv.writer(supercatfile)
    linkwriter = csv.writer(linkfile)
    redwriter = csv.writer(redirectfile)

    processed = 0

    for art in ArticleIterator(xmlfile):
        if art.ns == 14:
            catwriter.writerow([art.title] + art.getCategories())
        elif art.ns == 0:
            if art.redirect:
                redwriter.writerow([art.id, str(index.getID(art.redirect))])
            else:
                smallindex.write(f":{art.id}:{art.title}\n")
                links = art.getLinks()
                if links:
                    linkwriter.writerow([art.title, art.id, *[str(index.getID(link.target)) + (f"|{link.text}" if link.text else "") for link in links]])
        processed += 1
        if processed % 100000 == 0:
            print(f"{processed} Articles processed")
    print(f"Total {processed} Articles processed")

    supercatfile.close()
    linkfile.close()
    redirectfile.close()
    xmlfile.close()
    smallindex.close()

if __name__ == "__main__":
    pathWikiXML = sys.argv[1]
    pathIndex = sys.argv[2]
    pathCats = sys.argv[3]
    pathRedirects = sys.argv[4]
    pathLinksTo = sys.argv[5]
    pathSmallIndex = sys.argv[6]
    main(pathWikiXML, pathIndex, pathCats, pathRedirects, pathLinksTo, pathSmallIndex)
