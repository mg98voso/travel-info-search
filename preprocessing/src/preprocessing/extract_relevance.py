#extracts relevant articles from the wikipedia xml

import bz2
import xml.etree.ElementTree as etree
import codecs
import sys
import re
import csv
import gzip
from wikiparser import Article, ArticleIterator

def main(pathWikiXML, pathCats, pathOut, pathGT):
    
    integer = "[0-9]+"
    decimal = "-?[0-9\.]+"
    string = "'[^']*'"

    pattern = f"\({integer}\,({integer})\,'earth'\,1\,({decimal}|NULL)\,({decimal}|NULL)\,({integer}|NULL)\,({string}|NULL)\,({string}|NULL)\,({string}|NULL)\,({string}|NULL)\)"

    with gzip.open(pathGT, "rt") as f:
        text = f.read()

    gts = re.findall(pattern, text)
    geotags = dict()
    for ID, primary, *rest in gts:
        geotags[int(ID)] = 2
    
    MAX_LEVEL = 9
    
    supercatfile = open(pathCats, "r", newline="")
    csvreader = csv.reader(supercatfile)

    supercats = dict()
    subcats = dict()


    for catname, *superlist in csvreader:
        catname = re.match(".*\:(.*)", catname).group(1)
        supercats[catname] = superlist
        for supercat in superlist:
            subcats[supercat] = subcats.get(supercat, []) + [catname]
                

    supercatfile.close()

    def getCatLevel(catlist, alias):
        if alias in catlist:
            for cat in catlist:
                match = re.match(alias+r"\|([0-9]+)$", cat)
                if match:
                    return int(match.group(1))
        else:
            return 0
            
    def updateCatLevel(catlist, alias, level):
        if alias in catlist:
            for cat in catlist.copy():
                match = re.match(alias+"\|([0-9]+)", cat)
                if match:
                    if int(match.group(1)) > level:
                        catlist.remove(cat)
                        catlist.append(f"{alias}|{level}")
                        return True
                    else:
                        return False
        else:
            catlist += [alias, f"{alias}|{level}"]
            return True


    #mark all subcategories of a list of categories
    def reflexiveSupercat(catlist, alias, level=1):
        if level <= MAX_LEVEL:
            for cat in catlist:
                if not cat in supercats:
                    supercats[cat] = []
                if updateCatLevel(supercats[cat], alias, level):
                    if cat in subcats:
                        reflexiveSupercat(subcats[cat], alias, level+1)


    geocats = ["Geography by place", "Country subdivisions by administrative level"]
    poicats = ["Tourist attractions"]

    reflexiveSupercat(geocats, "geocat")
    reflexiveSupercat(poicats, "poicat")


    def hasSupercat(alias, catlist):
        for cat in catlist:
            if cat in supercats:
                if alias in supercats[cat]:
                    return getCatLevel(supercats[cat], alias)
        return False


    def strip_tag_name(t):
        idx = k = t.rfind("}")
        if idx != -1:
            t = t[idx + 1:]
        return t

    xmlfile = bz2.open(pathWikiXML)

    outfile = open(pathOut, "w", newline="")

    csvwriter = csv.writer(outfile)
    #csvwriter.writerow(["ID", "TITLE", "TYPE", "TEXT", "TAGS", "CONTENT"])


    total_count = 0
    noredirect_count = 0
    coords_count = len(geotags)
    georelevant = 0
    geocoords = 0
    poirelevant = 0
    poicoords = 0
    geopoi = 0
    extracted = 0

    csvwriter.writerow(["id", "hascoords", "georel", "poirel"])    
    
    for art in ArticleIterator(xmlfile):
        if art.ns == 0:
            total_count+=1
            if not art.redirect:
                noredirect_count += 1
                
                georel = hasSupercat("geocat", art.getCategories())
                poirel = hasSupercat("poicat", art.getCategories())
                
                if georel or poirel:
                    if art.id in geotags:
                        hascoords = 2
                    elif "{{coords" in art.text or "{{Coords" in art.text:
                        hascoords = 1
                    else:
                        hascoords = 0
                
                if georel:
                    georelevant += 1
                    if hascoords:
                        geocoords +=1
                
                if poirel:
                    poirelevant += 1
                    if hascoords:
                        poicoords +=1
                if georel and poirel:
                    geopoi += 1
                if georel or poirel:
                    csvwriter.writerow([art.id, hascoords, georel, poirel])
                    extracted += 1
                if noredirect_count % 100000 == 0:
                    print(noredirect_count, " Articles processed")
                    
    outfile.close()

if __name__ == "__main__":
    pathWikiXML = sys.argv[1]
    pathCats = sys.argv[2]
    pathOut = sys.argv[3]
    pathGT = sys.argv[4]
    main(pathWikiXML, pathCats, pathOut, pathGT)
