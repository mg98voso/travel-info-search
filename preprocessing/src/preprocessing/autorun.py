# manages preprocessing

import os
import extract_meta
import process_links
import merge_duplicates
import extract_relevance
import expand_relevants
import compose
import sys

DATAPATH = '/data'
workdir = '/data/preprocessing_data'
if len(sys.argv) == 4:
    dumpfile, indexfile, gtfile = sys.argv[1:]
else:
    print("\nTrying to locate data...")
    dumpfile, indexfile, gtfile = None, None, None
    candidates = os.listdir(DATAPATH)
    for candidate in candidates:
        if not dumpfile and candidate.endswith('xml.bz2'):
            dumpfile = candidate
            print("Corpus: ", dumpfile)
        elif not indexfile and candidate.endswith('index.txt.bz2'):
            indexfile = candidate
            print("Index: ", indexfile)
        elif not gtfile and candidate.endswith('geo_tags.sql.gz'):
            gtfile = candidate
            print("Geotags: ", gtfile)
if dumpfile and indexfile and gtfile:
    dump_path = os.path.join(DATAPATH, dumpfile)
    index_path = os.path.join(DATAPATH, indexfile)
    gt_path = os.path.join(DATAPATH, gtfile)
else:
    print("Could not find Wikipedia-dump. Please specify corpusfile, indexfile, geotags-file")
    exit()

catfile = "categories.csv"
redfile1 = "redirects_unfinished.csv"
linkstofile1 = "linksto_unfinished.csv"
sindexfile = "small_index.txt"

linkstofile = "linksto.csv"
linksfromfile1 = "linksfrom_unfinished.csv"
aliasfile1 = "aliases_unfinished.csv"

linksfromfile = "linksfrom.csv"
aliasfile = "aliases.csv"

relfile = "relevance.csv"

exfile = "expanded_relvs.csv"

outpath = "/data/corpus/articles.csv"

path = lambda name: os.path.join(workdir, name)
print("\nExtracting links and categories...\n")
extract_meta.main(dump_path, index_path, path(catfile), path(redfile1), path(linkstofile1), path(sindexfile))
print("\nProcessing links...\n")
process_links.main(index_path, path(redfile1), path(linkstofile1), path(linkstofile), path(linksfromfile1), path(aliasfile1))
print("\nCleaning up links...\n")
merge_duplicates.main(path(linksfromfile1), path(linksfromfile))
print("\nCleaning up aliasnames...\n")
merge_duplicates.main(path(aliasfile1), path(aliasfile))
print("\nComputing relevance-assumptions based on category-distance...\n")
extract_relevance.main(dump_path, path(catfile), path(relfile), gt_path)
print("\nAdding names of relevant higher-order articles...\n")
expand_relevants.main(index_path, path(relfile), path(linksfromfile), path(exfile))
print("\nMerging everything together...\n")
compose.main(dump_path, path(aliasfile), path(exfile), outpath)
print("\nPreprocessing complete.\n")
