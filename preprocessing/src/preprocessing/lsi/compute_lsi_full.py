from whoosh.analysis import StemmingAnalyzer
from gensim.parsing.preprocessing import STOPWORDS
from gensim.models.lsimodel import LsiModel
from gensim.corpora.dictionary import Dictionary
from .wikiparser import ArticleIterator
import bz2

import csv
import sys

csv.field_size_limit(sys.maxsize)

corpus = bz2.open('/media/matze/TOSHIBA EXT/enwiki-20190501-pages-articles-multistream.xml.bz2')
lsi = open('/media/matze/TOSHIBA EXT/tis_data/lsi_full.csv', "w")

writer = csv.writer(lsi)

model = LsiModel.load('/media/matze/TOSHIBA EXT/relevant_wikitopics/model')
d = Dictionary.load('/media/matze/TOSHIBA EXT/relevant_wikitopics/wikidictionary')

tokenize = StemmingAnalyzer(stoplist = STOPWORDS)
count = 0


for article in ArticleIterator(corpus):
    if article.ns == 0 and not article.redirect:
        bow = d.doc2bow([token.text for token in tokenize(article.text)])   # markup will not be removed explicitly
        vec = model[bow]
        values = [str(item[1]) for item in sorted(vec, key=lambda i:i[0])]
        writer.writerow([article.id] + values)
        count += 1
        print("\033[H\033[J")
        print(f"{count} Articles processed.")

corpus.close()
lsi.close()
