from gensim.corpora.dictionary import Dictionary
from gensim.models.lsimodel import LsiModel

from gensim.corpora.textcorpus import TextCorpus
from whoosh.analysis import StemmingAnalyzer

from gensim.parsing.preprocessing import STOPWORDS
import csv
import sys

csv.field_size_limit(sys.maxsize)

class RelWikicorpus(TextCorpus):

    def getstream(self):
        num_texts = 0
        self.input.seek(0)
        inputreader = csv.reader(self.input)
        for ID, TITLE, ALT, TYPE, SUMMARY, TEXT, TAGS, CONTENT, META in inputreader:
            num_texts += 1
            if num_texts % 100000 == 0:
                print(f"{num_texts} gelesen")
            yield '\n'.join([TITLE, ALT, SUMMARY, TEXT, TAGS, CONTENT])
        self.length = num_texts
    
    def get_texts(self):
        tokenize = StemmingAnalyzer(stoplist = STOPWORDS)
        for article in self.getstream():
            yield [token.text for token in tokenize(article)]
    
    def __len__(self):
        if not self.length:
            self.length = sum(1 for _ in self.get_texts())
        return self.length

dump = open("/media/matze/TOSHIBA EXT/tis_data/articles.csv", "r", newline='')


print("Building corpus...")

corpus = RelWikicorpus(dump, dictionary=Dictionary.load("/media/matze/TOSHIBA EXT/relevant_wikitopics/wikidictionary"))
print("Saving dictionary...")
corpus.dictionary.save("/media/matze/TOSHIBA EXT/relevant_wikitopics/wikidictionary")

print("Training model...")
model = LsiModel(corpus, num_topics=300, id2word=corpus.dictionary)

print("Saving model...")
model.save("/media/matze/TOSHIBA EXT/relevant_wikitopics/model")

dump.close()
