from whoosh.analysis import StemmingAnalyzer
from gensim.parsing.preprocessing import STOPWORDS
from gensim.models.lsimodel import LsiModel
from gensim.corpora.dictionary import Dictionary

import csv
import sys

csv.field_size_limit(sys.maxsize)

corpus = open('/media/matze/TOSHIBA EXT/tis_data/articles.csv')
lsi = open('/media/matze/TOSHIBA EXT/tis_data/lsi.csv', "w")

reader = csv.reader(corpus)
writer = csv.writer(lsi)

model = LsiModel.load('/media/matze/TOSHIBA EXT/relevant_wikitopics/model')
d = Dictionary.load('/media/matze/TOSHIBA EXT/relevant_wikitopics/wikidictionary')

tokenize = StemmingAnalyzer(stoplist = STOPWORDS)
count = 0

next(reader)

for ID,TITLE,ALT,TYPE,SUMMARY,TEXT,TAGS,CONTENT,META in reader:
    bow = d.doc2bow([token.text for string in [TITLE, ALT, SUMMARY, TEXT, TAGS, CONTENT] for token in tokenize(string)])
    vec = model[bow]
    values = [str(item[1]) for item in sorted(vec, key=lambda i:i[0])]
    writer.writerow([ID] + values)
    count += 1
    print("\033[H\033[J")
    print(f"{count} Articles processed.")

corpus.close()
lsi.close()
