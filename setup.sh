 #!/bin/sh
 
./preprocess.sh
cd backend/
docker-compose up
docker-compose exec backend python3 manage.py create_index_whoosh --path /code/data/corpus/articles.csv
