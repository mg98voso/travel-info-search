Note:To this point, the values were all 1.0, except for TEXT which had 0.1

every value == 1.0
Results: A few better matches regarding both GEO and POI.

TITLE = 1.5
Results: No significant changes.

TITLE = 0.75
Results: A few better POI results.

TITLE = 0.45
Results: Better results regarding POI.

ALT = 1.5
Results: A few different results, but none of better GEO or POI.

ALT = 0.75
Results: Two better results regarding POI.

ALT = 0.45
Results: Better results regarding GEO and POI.

TEXT = 1.5
Results: A lot different results regarding both POI and GEO, but just a few with relevance 2. 

SUMMARY = 1.5
Results: A few new results, most with relevance 2, both GEO and POI.

SUMMARY = 0.75
Results: No changes.

SUMMARY = 0.45
Results: A few new results with relevance 1, one with relevance 2. 

TAGS = 1.5
Results: Two new results, relevance 0 and 1.

TAGS = 0.75
Results: No changes.

TAGS = 0.45
Results: No changes.

CONTENT = 1.5
Results: One new result with relevance 0.

CONTENT = 0.75
Results: No changes.

CONTENT = 0.45
Results: No changes.

META = 1.5
Results: A few different results, just one of relevance 2.

META = 0.75
Results: No changes.

META = 0.45
Results: No changes.

Note: Now combining the best results.

TITLE = 0.45 AND ALT=0.45
Results: One better result with relevance 2.

TITLE = 0.45 and ALT=0.45 and SUMMARY = 0.45
Results: A few different results with 0 and 1 relevances.

TITLE = 0.45 and ALT=0.45 and SUMMARY = 0.45 and TEXT = 0.1
Results:A lot new results, most with relevance 1, few with relevance 2.

TITLE = 0.45 and ALT=0.45 and SUMMARY = 0.45 and TEXT = 0.1
Results: Both better and worse results, few more with relevance 2, 1, and 0.

###########################################################################
New evaluation after changes on 14.08.2019 (see changelog on git)

Every value = 1.0
Results: Many better results, most of it relevance 2, none with relevance 0.

TITLE = 0.45
Results: A few better results, equal amount of relevance 1 and 2.

ALT = 0.45
Results: A few new results, every result relevance 1.

SUMMARY = 0.45
Results: A few new results, every result relevance 1.

TEXT = 0.1
Results: A lot better results with relevance 1 and 2.

SUMMARY = 0.45 and TEXT = 0.1
Results: A few better results, all relevance 2.

