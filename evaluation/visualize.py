import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from os import listdir


def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.1f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')

if len(sys.argv) > 1:   # compare the specified evaluations topic by topic
    jsons = sys.argv[1:]
    stats = dict()
    for fn in jsons.copy():
        with open(fn) as file:
            judgements = json.load(file)
            if 'relevance' in judgements[0]:    # check whether the file is an evaluation-json or not
                topics = dict()
                for j in judgements:
                    tid = j['topic_id']
                    if not tid.isnumeric():
                        tid = tid[1:]
                    topics[tid] = topics.get(tid, []) + [j['relevance']]
                for i in range(1, 51):
                    if not str(i) in topics:
                        topics[i] = []
                stats[fn] = topics
    labels = list(range(1, 51))
    topicPrec = lambda l: len([x for x in l if x == "2"]) / len(l) if l else 0
    precisions = [[topicPrec(stats[fn][topic]) for topic in sorted(stats[fn].keys(), key=lambda t:int(t))] for fn in sorted(stats.keys())]
    x = np.arange(len(labels))  # the label locations
    width = 0.25  # the width of the bars
    relative_positions = [(i-(len(stats)/2))*width for i in range(len(labels))]
    fig, ax = plt.subplots()
    rects = [ax.bar(x + pos, precs, width, label=label) for label, precs, pos in zip(sorted(stats.keys()), precisions, relative_positions)]
    ax.set_ylabel('precision')
    ax.set_title('Precisions by configuration and topic')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    
    for rect in rects:
        autolabel(rect)
        
    fig.tight_layout()
    
    plt.show()

else:   # compare all evaluation-jsons located in the current folder
    jsons = [fn for fn in listdir() if fn.endswith(".json")]
    stats = dict()
    for fn in jsons.copy():
        with open(fn) as file:
            judgements = json.load(file)
            if 'relevance' in judgements[0]:    # check whether the file is an evaluation-json or not
                topics = dict()
                for j in judgements:
                    tid = j['topic_id']
                    if not tid.isnumeric():
                        tid = tid[1:]
                    topics[tid] = topics.get(tid, []) + [j['relevance']]
                for i in range(1, 51):
                    if not str(i) in topics:
                        topics[i] = []
                topicPrec = lambda l: len([x for x in l if x == "2"]) / len(l) if l else 0
                stats[fn] = sum([topicPrec(l) for t, l in topics.items()]) / len(topics)
    sorted_stats = sorted(stats.items(), key=lambda i:i[1])
    names = [name for name, prec in sorted_stats]
    precs = [prec for name, prec in sorted_stats]
    y_pos = np.arange(len(names))
    plt.rcdefaults()
    fig, ax = plt.subplots()
    ax.barh(y_pos, precs)
    ax.set_yticks(y_pos)
    ax.set_yticklabels(names)
    ax.set_xlabel('Precision')
    plt.show()

