Semi-automatic evaluation:

run `sudo docker-compose exec backend python3 manage.py evaluate_topics --topics evaluation/topicsAll.json --evaluations evaluation/ [--outputname <filename for output>]`

The script will read all previous evaluations and automatically assign results their respective relevance-value if one has been previously assigned. (Conflicting assignments in existing files are not expected, the script will simply pick one)
