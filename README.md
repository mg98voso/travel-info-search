**How to set up and start the backend:**

(make sure that following packages are installed: can-utils, curl)

1. Install docker-compose as described here https://docs.docker.com/compose/install/
2. Restart your System
3. Go to the git root
4. `cd backend/`
5. `docker-compose up`


**How to prepare the corpus**

1. Go to https://dumps.wikimedia.org/enwiki/
   * Get "enwiki-xxxxxxxx-pages-articles-multistream.xml.bz2"
   * Get "enwiki-xxxxxxxx-pages-articles-multistream-index.txt.bz2"
   * Get "enwiki-xxxxxxxx-geo_tags.sql.gz"
2. copy the files to data/
3. `./preprocess.sh`
4. Wait a few hours.

Or download the preprocessed corpus used in development:
* https://www.file-upload.net/download-13701988/articles.zip.html (md5sum: 8fcec9348964c17e23b75877ecb1fdb9)
* Unzip the file and copy it to travel-info-search/data/corpus/



**How to build the index:**

1. Go to "git root"/backend
2. Make sure that the docker container is running
3. `docker-compose exec backend python3 manage.py create_index_whoosh --path /code/data/articles.csv`


**Or do preprocessing and indexing in one run:**
1. `./setup.sh` (don't execute `docker-compose up` beforehand)


**Access the Frontend:**

1. Open a browser and got to "localhost:8000"


HAVE FUN!
